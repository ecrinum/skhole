Site construit avec [Hugo](https://gohugo.io/), suivant les conventions de ce logiciel.

## Conventions et organisation des fichiers

```shell
├── LICENSE
├── README.md
├── archetypes/     # gabarits pour les nouvelles pages 📐
├── config.toml     # fichier de configuration Hugo ⚙️
├── content         # répertoire des pages sources 📝
│   ├── _index.md   # page d’accueil 🏠
│   └── episodes/   # sous-répertoire des pages des balados ⭐️
│   ├── page.md     # page.md => `/page/`
├── static          # fichiers statiques (seront copiés à la racine du site)
│   ├── favicon.ico
│   ├── images/
│   └── medias/     # répertoire des fichiers audio 🎧
└── themes/         # thèmes hugo
```

## Nomenclature des fichiers

Épisodes : snakecase --> "ep" suivi du numéro de l’épisode underscore "nom de famille de l’invité·e"
Ex. : ep1_mellet.mp3

## Configuration de Git LFS
Git LFS permet de versionner des fichiers de façon partielle afin de ne pas alourdir le dépôt.
Pour une interaction en local il est nécessaire d'installer Git LFS : `sudo apt install git-lfs`.

## Métadonnées pour les balados

Dans le préambule (<em lang="en">front-matter</em>) du fichier markdown d’une page de balado, insérer les propriétés suivantes :

~~~yaml
title: ""

numero: 

# date de publication du balado au format universel
date: 

# description brève (~150 caractères)
description: ""

# paragraphe de présentation (multiligne accepté)
description_longue: |
  Exemple de description longue...

# étiquettes -- attention de se conformer aux majuscules/minuscules!
tags: []

# URL pour lecteur Spotify embarqué
spotify_embed_url: ""

# chemin vers le fichier mp3
mp3: "/medias/episode.mp3"

# taille en octets pour le fichier mp3 (facultatif)
length: ""

# URL complet pour Apple Podcasts
apple_url: ""
~~~
Pour l’URL Spotify, choisir le src dans le code du embed complet, pas l'URL de partage.

Il est possible de recourir à la ligne de commande pour générer une nouvelle page avec ces métadonnées :

```shell
# `X` correspond au numéro d’épisode
hugo new episodes/X-slug-episode-nom-auteur.md
```

## Licence

Le code est placé sous licence MIT (voir le fichier [LICENSE](LICENSE)).

Les contenus (dossier [`content`](content) et [`static`](static)) sont publiés sous licence [Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr).
