---
title: ""

numero: {{ replaceRE "^ep([0-9]+)-.+" "$1" .Name }}{{/* ep00_episode.md => 00 */}}

date: {{ .Date }}

# description brève (~150 caractères)
description: ""

# paragraphe de présentation (multiligne accepté)
description_longue: |

# étiquettes -- attention de se conformer aux majuscules/minuscules!
tags: []

# URL pour lecteur Spotify embarqué
spotify_embed_url: ""

# chemin vers le fichier mp3
mp3: "/medias/episode.mp3"

# taille en octets pour le fichier mp3 (facultatif)
length: ""

# URL complet pour Apple Podcasts
apple_url: ""
---
