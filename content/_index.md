---
---

Σχολή (Skholé) est une baladodiffusion conçue par la [Chaire de recherche du Canada sur les écritures numériques](https://ecrituresnumeriques.ca/fr/), qui explore l’idée que la théorie ne sert à rien, si ce n’est qu’elle a l’avantage de nous faire perdre beaucoup de temps. Dans chaque épisode, un·e invité·e du milieu de la recherche scientifique ou universitaire présente un concept théorique en répondant à ces trois questions : 

1. Comment définissez-vous le concept présenté ? Donnez-en une définition simple et une définition complexe.

2. Quels sont des exemples de ce concept ?

3. En quoi le concept ne sert-il à rien ? Expliquez comment il nous fait dysfonctionner et perdre du temps.

Aussi Σχολή s’inscrit-elle dans la valorisation du savoir contemplatif et non productif, et fait-elle l’éloge du dysfonctionnement.
