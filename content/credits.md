---
title: "Crédits"
---

Σχολή (Skholé) est une baladodiffusion de l'initiative de la [Chaire de recherche du Canada sur les écritures numériques](https://ecrituresnumeriques.ca/fr/).

Conception des épisodes : [Yann Audin](https://ecrituresnumeriques.ca/fr/Equipe/Yann-Audin) et [Arilys Jia](https://ecrituresnumeriques.ca/fr/Equipe/Arilys-Jia)

Montage : Yann Audin

Thème musical composé par Arilys Jia, monté par Yann Audin, avec les voix de [Louis-Olivier Brassard](https://www.loupbrun.ca/), [Alix Chagué](https://alix-tz.github.io/), [Roch Delannay](https://ecrituresnumeriques.ca/fr/Equipe/Roch-Delannay), [Antoine Fauchié](https://ecrituresnumeriques.ca/fr/Equipe/Antoine-Fauchie), [Giulia Ferretti](https://ecrituresnumeriques.ca/fr/Equipe/Giulia-Ferretti), [Arilys Jia](https://ecrituresnumeriques.ca/fr/Equipe/Arilys-Jia), [Eugénie Matthey-Jonais](https://ecrituresnumeriques.ca/fr/Equipe/Eugenie-Matthey-Jonais), [Margot Mellet](https://ecrituresnumeriques.ca/fr/Equipe/Margot-Mellet), [Mathilde Verstrate](https://ecrituresnumeriques.ca/fr/Equipe/Mathilde-Verstraete) et [Marcello Vitali-Rosati](https://ecrituresnumeriques.ca/fr/Equipe/Marcello-Vitali-Rosati).

Ce site a été réalisé avec les instances suivantes : 

- [Hugo](https://gohugo.io/) et le thème [Archie](https://themes.gohugo.io/themes/archie/), créé par [Athul Cyriac Ajay](https://github.com/athul)
- Script de la bannière de [Letters Animation](https://codepen.io/infasyskey/pen/gOYBygE), créé par [Carlos Del Cura Pascual](https://codepen.io/infasyskey)

et 

- [Margot Petites Mains](https://blank.blue/)
- [Antoine Fauchié](https://www.quaternum.net/)

avec les outils : 

- [VsCodium](https://vscodium.com/) 
- [Gitlab HumaNum](https://gitlab.huma-num.fr/)

