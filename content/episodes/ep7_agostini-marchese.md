---
title: "Le *worldmaking* - Enrico Agostini-Marchese"
numero: 7
date: 2025-02-06T12:47:00+05:30

# description brève (~150 caractères)
description: "Worldmaking, positionnement, responsabilité"

# paragraphe de présentation (multiligne accepté)
description_longue: |
   Dans l’ouvrage *Ways of Worldmaking*, Nelson Goodman présente le concept éponyme de *worldmaking*, que l’on pourrait traduire par construction de mondes ou fait de « faire monde ». Les mondes sont aussi nombreux que les sources qui les produisent et sont exprimés dans des systèmes symboliques, tels que les discours ou les représentations visuelles. Dans cet épisode, Enrico Agostini-Marchese se réfère à ce concept pour parler de pluralisation de visions du monde, d’inclusivité et de responsabilisation.

# étiquettes
tags: [collaboration, femmes, resistance, monde, collectif, espace, sujet, systeme]
# chemin vers le fichier mp3
mp3: "/medias/ep7_agostini-marchese.mp3"
# taille en octets pour le fichier mp3 (facultatif)
length: "33407507"
# URL pour lecteur Spotify embarqué
spotify_embed_url: "https://open.spotify.com/embed/episode/4VIFzp2IJefDiZ9lj5mG4h?utm_source=generator"
# URL complet pour Apple Podcasts
apple_url: "https://embed.podcasts.apple.com/us/podcast/7-le-worldmaking-enrico-agostini-marchese/id1689113816?i=1000689594131"
---

## Biographie

Enrico Agostini-Marchese est professionnel de recherche pour le Centre de recherche interdisciplinaire sur la justice intersectionnelle, la décolonisation et l’équité (CRI-JaDE), où iel s’occupe de la mobilisation des connaissances. Titulaire d’une maîtrise en philosophie (Université de Florence), spécialisation en esthétique, et d’un doctorat en littératures de langue française (Université de Montréal), spécialisation en littérature numérique, ses recherches portent sur la place que l’imaginaire et les pratiques artistiques ont dans la production de l’espace. Sa thèse a gagné le prix pour la meilleure thèse en Arts, lettres et sciences humaines en 2021. Pendant son post-doctorat, iel s’est intéressé·e aux approches queer et décoloniales, qu’iel applique maintenant dans la recherche et dans la vie de tous les jours.

## Références

Ahmed, Sara. 2006. « The Nonperformativity of Antiracism ». *Meridians* 7 (1): 104‑26.

———. 2007. *Queer Phenomenology: Orientations, Objects, Others*. 2. printing. Durham: Duke University Press.

Appadurai, Arjun. 1996. *Modernity at Large: Cultural Dimensions of Globalization*. Public Worlds, v. 1. Minneapolis, Minn: University of Minnesota Press.

Barad, Karen Michelle. 2007. *Meeting The Universe Halfway: Quantum Physics and The Entanglement of Matter and Meaning*. Durham: Duke University Press.

Deleuze, Gilles. 1968. *Différence et répétition*. Paris: PUF.

Deleuze, Gilles, et Félix Guattari. 1975. *Kafka : pour une littérature mineure.* Collection « Critique ». Paris: Éditions de Minuit.

Escobar, Arturo. 2008. *Territories of Difference: Place, Movements, Life,* Redes. New Ecologies for The Twenty-First Century. Durham: Duke University Press.

———. 2018. *Designs for The Pluriverse: Radical Interdependence, Autonomy, and the Making of Worlds*. New Ecologies for the Twenty-First Century. Durham: Duke University Press.

Goodman, Nelson. 2013. *Ways of Worldmaking*. 11. pr. Hackett Classic 51. Indianapolis, Ind: Hackett.

Haraway, Donna. 1988. « Situated Knowledge. The Science Question in Feminism and the Privilege of Partial Perspective ». *Feminist Studies* 14 (3): 575‑99.

hooks, bell. 2019. *Apprendre à transgresser*. Montréal: M éditeur.

Latour, Bruno. 2007. « Paris, la ville invisible : le plasma ». In *Airs de Paris, 30 ans du Centre Pompidou*. Paris: ADGP. http://www.bruno-latour.fr/sites/default/files/P-123-BEAUBOURG-PARIS.pdf.

Schmitt, Carl. 1985. *Terre et mer : un point de vue sur l’histoire mondiale*. Éditions du Labyrinthe. Paris.