---
title: "Building 21 - Ollivier Dyens"
numero: 5
date: 2024-09-16T06:30:00+05:30

# description brève (~150 caractères)
description: "Building 21 : un espace de recherche multidisciplinaire"

# paragraphe de présentation (multiligne accepté)
description_longue: |
    Dans cet épisode, Ollivier Dyens présente le laboratoire [Building 21](https://www.building21.ca) de l’Université McGill, un lieu de recherche atypique, inter- et transdisciplinaire, qui met à l’épreuve le cloisonnement institutionnel des disciplines. Cet espace, dédié aux processus plutôt qu’aux résultats, accueille chaque année une dizaine de boursier·ère·s et d’invité·e·s qui prennent des risques avec des sujets de recherche invitant à la réflexion collective et explorant la porosité des domaines de la connaissance.

# étiquettes
tags: [recherche atypique, skhole, espace creatif, laboratoire, collaboration]
# chemin vers le fichier mp3
mp3: "/medias/ep5_dyens.mp3"
# taille en octets pour le fichier mp3 (facultatif)
length: "78161573"
# URL pour lecteur Spotify embarqué
spotify_embed_url: "https://open.spotify.com/embed/episode/39Vl0CRaLFFkvxJS3ID99L?utm_source=generator"
# URL complet pour Apple Podcasts
apple_url: "https://podcasts.apple.com/ca/podcast/5-building-21-ollivier-dyens/id1689113816?i=1000669750688"
---

## Biographie

Ollivier Dyens est professeur titulaire du Département des littératures de langue française, de traduction et de création de l’Université McGill, et codirecteur de Building 21. Il était membre du Conseil supérieur de l’éducation, de 2011 à 2015, et Premier vice-recteur exécutif adjoint aux études et à la vie étudiante de l’Université McGill de 2011 à 2015. Ollivier Dyens est diplômé de l’Université de Montréal en littérature comparée, et est l’auteur de *Les jardins tordus*, *La terreur et le sublime*, *Metal and Flesh*, *La Condition inhumaine* et plusieurs autres essais, monographies et recueils de poésie. Ses recherches portent sur les études posthumaines, le cinéma, la poésie et les cybercultures, et ses créations numériques ont été exposées au Brésil, au Canada, en France, au Venezuela, en Allemagne, en Argentine et aux États-Unis.