---
title: "Le bug - Marcello Vitali-Rosati"
numero: 4
date: 2024-05-20T02:01:58+05:30

# description brève (~150 caractères)
description: "Éloge du bug et du dysfonctionnement"

# paragraphe de présentation (multiligne accepté)
description_longue: |
  « Ça marche, c’est tout », nous dit la publicité pour un smartphone. C’est simple, c’est intuitif. Il n’y a pas à se poser de questions. Le passage au numérique tel que nous le vivons aujourd’hui correspond largement à une délégation généralisée des choix politiques, éthiques, culturels et sociaux à des opérateurs privés qui ont su rapidement proposer des « solutions fonctionnelles » pour à peu près tout. Dans l’essai provocateur *Éloge du bug*, Marcello Vitali-Rosati prend le contre-pied de cet « impératif fonctionnel » au coeur de la vision du monde dans laquelle les GAFAM nous enferment. Paru aux éditions [Zones](https://www.editions-zones.fr/livres/eloge-du-bug/) chez la Découverte et disponible gratuitement [en lecture en ligne](https://www.editions-zones.fr/lyber?eloge-du-bug) et en [PDF](https://papyrus.bib.umontreal.ca/xmlui/handle/1866/33126).

  Le bug : insecte, dysfonctionnement informatique, blâme porté par un système fautif, que révèle cette notion sur nos paradigmes épistémologiques et socioéconomiques ? Dans cet épisode, nous examinons la place du dysfonctionnement dans nos systèmes sans cesse optimisés pour la productivité.

# étiquettes
tags: [bug, dysfonctionnement, skhole, resistance]
# chemin vers le fichier mp3
mp3: "/medias/ep4_vitali-rosati.mp3"
# taille en octets pour le fichier mp3 (facultatif)
length: "29337055"
# URL pour lecteur Spotify embarqué
spotify_embed_url: "https://podcasters.spotify.com/pod/show/ecrinum/embed/episodes/Le-bug--Marcello-Vitali-Rosati-e2jrq91/a-ab9hev2"
# URL complet pour Apple Podcasts
apple_url: "https://podcasts.apple.com/ca/podcast/4-le-bug-marcello-vitali-rosati/id1689113816?i=1000656080004"
---

## Biographie

Philosophe et spécialiste d'édition numérique, Marcello Vitali-Rosati est professeur au département des littératures de langue française de l'Université de Montréal et titulaire de la Chaire de recherche du Canada sur les écritures numériques. Il développe une réflexion philosophique sur ce que devient le monde à l'ère des technologies numériques. À partir de l'étude et de la pratique du code, il analyse la manière dont les algorithmes, les formats, les logiciels et les plateformes redéfinissent les notions d'humain, d'identité, de connaissance ou de littérature. Contributeur actif à la théorie de l'éditorialisation, il travaille à la conception de nouvelles formes de production et de diffusion du savoir ainsi qu'à l'élaboration de chaînes éditoriales innovantes. Il est l'auteur de nombreux articles et monographies et exerce également une activité d'éditeur en tant que directeur de la revue Sens public et co-directeur de la collection « Parcours numériques » aux Presses de l'Université de Montréal. Il est à la tête de plusieurs projets en humanités numériques, particulièrement dans le domaine de l'édition savante: des plateformes d'édition de revues et de monographies enrichies, de l'éditeur de texte Stylo ainsi que d'une plateforme d'édition collaborative de l'Anthologie grecque.

## Ressources et références

Vitali-Rosati, Marcello, *Éloge du bug*, Paris, Zones, 2024.

[Bibliothèque Zotero](https://www.zotero.org/groups/critures_numriques/items/order/year/q/Vitali-Rosati/sort/desc)
