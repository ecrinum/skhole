---
title: "Les « petites mains » - Margot Mellet"
numero: 1
date: 2023-05-31T02:01:58+05:30

# description brève (~150 caractères)
description: "Des petites mains qui prennent la tête."

# paragraphe de présentation (multiligne accepté)
description_longue: |
  Couturières, secrétaires, ouvrières, nombre de petites mains&mdash;&nbsp;qu’elles soient l’outil de travail par excellence dans l’accomplissement de tâches répétitives ou dévaluées, ou qu’elles désignent, métonymiquement, celles qui les agitent&nbsp;&mdash; ont contribué et contribuent, avec une visibilité variable, au développement de la culture et du savoir. Aujourd’hui, nous explorons le travail invisibilisé dans les modèles de collaboration scientifique et posons la question&nbsp;: «&nbsp;À *qui* doit-on vraiment la production de la connaissance&nbsp;? »

# étiquettes
tags: [fabrique, femmes]
# chemin vers le fichier mp3
mp3: "/medias/ep1_mellet.mp3"
# taille en octets pour le fichier mp3 (facultatif)
length: "59044153"
# URL pour lecteur Spotify embarqué
spotify_embed_url: "https://open.spotify.com/embed/episode/2hMsY28wRDR7DGNtdTA9T4?utm_source=generator"
# URL complet pour Apple Podcasts
apple_url: "https://podcasts.apple.com/ca/podcast/1-les-petites-mains-margot-mellet/id1689113816?i=1000614056112"
---

## Biographie

Margot Mellet est doctorante en littératures de langue française à l’Université de Montréal en recherche et création. Son projet de thèse est consacré au palimpseste comme cadre conceptuel pour définir la fabrique de la pensée. Entre herméneutique matérielle et réflexion sur les configurations techniques du texte à l’écran, sa création explore les possibilités plastiques de la littérature dans les environnements numériques d’écriture. Elle est également membre étudiante du CRIalt (Centre de recherches intermédiales sur les arts, les lettres et les techniques), membre du groupe de recherche *Comparative Materialities* de l’Association canadienne de littérature comparée et coordonnatrice scientifique de la Chaire de recherche du Canada sur les écritures numériques. Son travail et ses processus d’écriture sont disponibles sur son site : Blank. blue (https://blank.blue/).

## Ressources et références

[Bibliothèque Zotero](https://www.zotero.org/groups/4695868/petites_mains/library)
