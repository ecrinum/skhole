---
title: "Les protocoles scientifiques - Nicolas Sauret"
numero: 3
date: 2024-02-07T02:01:58+05:30

# description brève (~150 caractères)
description: "Le rôle des protocoles dans le processus scientifique"

# paragraphe de présentation (multiligne accepté)
description_longue: |
  Qu’est-ce qu’un protocole, au sens scientifique, et à quoi sert-il ?
  Structure rigide, prescriptive, universelle, ou bien vecteur de renouvellement de la recherche scientifique ? Quel rôle joue le protocole dans la recherche collaborative, la critique scientifique et la légitimation du savoir ? Dans cet épisode, nous nous penchons sur la manière de *faire science* autrement.

# étiquettes
tags: [protocole, collaboration, fabrique]
# chemin vers le fichier mp3
mp3: "/medias/ep3_sauret.mp3"
# taille en octets pour le fichier mp3 (facultatif)
length: "55557273"
# URL pour lecteur Spotify embarqué
spotify_embed_url: "https://open.spotify.com/embed/episode/7AFLWrP5C4dRZSZnnVKxLH?utm_source=generator"
# URL complet pour Apple Podcasts
apple_url: "https://podcasts.apple.com/ca/podcast/3-les-protocoles-scientifiques-nicolas-sauret/id1689113816?i=1000639228698"
---

## Biographie

Nicolas Sauret est maître de conférence en sciences de l'information et de la communication à l'Université Paris 8 Vincennes -- Saint-Denis où il enseigne au département « humanités numériques ».
Ses travaux de recherches portent sur les matérialités numériques de l'écriture et de l'édition. Il s'intéresse plus particulièrement aux écritures collectives qui se déploient dans l'environnement numérique, et aux dynamiques conversationnelles qui en découlent. Dans une approche de recherche-action, il explore les pratiques de différentes communautés d'écriture -- littérature numérique, communs, universités -- avec et pour lesquelles il co-conçoit des espaces ouverts et partagés d'écriture et d'édition. Ces travaux s'inscrivent dans une réflexion épistémologique sur les modes de production, de diffusion et de légitimation des connaissances dans l'environnement numérique.

## Ressources et références

Monjour, Servanne et Nicolas Sauret, « Pour une gittérature : l'autorité à l'épreuve du hack », *XXI/XX Reconnaissances littéraires*, nº 2, 2021, p. 237-252. ⟨10.48611/isbn.978-2-406-12363-7.p.0237⟩. ⟨hal-03962836⟩

---, « Hacking Protocols », communication présentée aux journées d’études « Écritures alternatives de la recherche en SHS : nouvelles stratégies, nouvelles pratiques, nouveaux formats », Maison des sciences de l’Homme Ange-Guépin et Centre de recherche nantais Architectures Urbanités, novembre 2023.

Sauret, Nicolas, « Ouvrir la gouvernance de la publication savante : “Écrire les communs” dans la revue *Sens public* », *in* « La revue collectif : écologie du savoir », thèse de doctorat en sciences de l’information et de la communication et littérature comparée, Nanterre et Montréal, Université Paris Nanterre et Université de Montréal, 2020, f. 304-326, <https://these.nicolassauret.net/1.0/revuecollectif.html#ouvrir-la-gourvernance-de-la-publication-savante-%C3%A9crire-les-communs-dans-la-revue-sens-public> (page consultée le 12 février 2024).