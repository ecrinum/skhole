---
title: "Skholé - Théories dysfonctionnelles"
numero: 0
date: 2023-05-24T02:01:58+05:30

# description brève (~150 caractères)
description: "Bienvenue à Σχολή (Skholé) !"

# paragraphe de présentation (multiligne accepté)
description_longue: |
  Σχολή (Skholé) est une baladodiffusion conçue par la Chaire de recherche du Canada sur les écritures numériques, qui explore l’idée que la théorie ne sert à rien, si ce n’est qu’elle a l’avantage de nous faire perdre beaucoup de temps ! Dans chaque épisode, un·e invité·e du milieu de la recherche scientifique ou universitaire présente une notion théorique tout en expliquant la manière dont elle nous oblige à ralentir ou à dysfonctionner, faisant ainsi l’éloge du savoir non productif et de la contemplation philosophique, ou de ce que les Grec·que·s appelaient la <i>skholé</i>.

# étiquettes
tags:
# chemin vers le fichier mp3
mp3: "/medias/trailer.mp3"
# taille en octets pour le fichier mp3 (facultatif)
length: "655049"
# URL pour lecteur Spotify embarqué
spotify_embed_url: "https://open.spotify.com/embed/episode/10zs48Ig6qXPOA9K8n9Lna?utm_source=generator"
# URL complet pour Apple Podcasts
apple_url: "https://podcasts.apple.com/ca/podcast/bande-annonce/id1689113816?i=1000614055913"
---
