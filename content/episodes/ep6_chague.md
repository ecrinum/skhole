---
title: "La transcription automatique de manuscrits - Alix Chagué"
numero: 6
date: 2024-10-25T12:47:00+05:30

# description brève (~150 caractères)
description: "Les pratiques de science ouverte dans la transcription automatique de manuscrits"

# paragraphe de présentation (multiligne accepté)
description_longue: |
    Les chercheur·euse·s qui font de l’analyse de manuscrits ont besoin de textes numérisés et traitables par ordinateur. Quand ils ne sont pas disponibles, il leur est nécessaire de recopier ces textes à la main. L’apprentissage automatique accélère grandement ces pratiques, mais de telles méthodes requièrent des exemples générés par des transcripteur·ice·s. Autrement dit, ces techniques ont besoin de données qui ne sont pas toujours accessibles aux chercheur·euse·s. Dans cet épisode, nous traitons de transcription automatique et de données ouvertes, ces dernières proposant une occasion de décloisonner les pratiques scientifiques.

# étiquettes
tags: [fabrique, collaboration, interoperabilite, laboratoire, protocole, donnees, science ouverte]
# chemin vers le fichier mp3
mp3: "/medias/ep6_chague.mp3"
# taille en octets pour le fichier mp3 (facultatif)
length: "72776169"
# URL pour lecteur Spotify embarqué
spotify_embed_url: "https://open.spotify.com/embed/episode/38B7kOI3yan75PYlmJM1L4?utm_source=generator"
# URL complet pour Apple Podcasts
apple_url: "https://podcasts.apple.com/ca/podcast/6-la-transcription-automatique-de-manuscrits-alix-chagu%C3%A9/id1689113816?i=1000674468302"
---

## Biographie

Alix Chagué est doctorante en humanités numériques au département de littérature et de langues du monde à l'Université de Montréal ainsi qu'à l'École Pratique des Hautes Études à Paris. Elle est aussi membre de l'équipe de recherche ALMAnaCH à Inria Paris depuis près de 7 ans, d'abord comme ingénieure puis comme doctorante. Ses recherches portent sur la transcription automatique appliquée aux documents historiques. En 2020, elle a cofondé le projet HTR-United dont l'objectif est de faciliter le partage des données pour l'entraînement des systèmes de transcription. Elle participe également à la création de communautés de pratiques et d'infrastructure pour le développement de cette technologie auprès des institutions patrimoniales et de la recherche en sciences humaines. Ses recherches sont abordées dans son [carnet](alix-tz.github.io/phd).

## Références

« Consistent Approaches to Transcribing ManuScripts », *CATMuS Guidelines*, https://catmus-guidelines.github.io/ (page accédée le 18 octobre 2024).

Chagué, Alix *et al.*, « Chaînes d’acquisition, de traitement et de publication du texte », Consortium Ariane - Axe 1, octobre 2024, https://hal.science/hal-04734959 (page accédée le 18 octobre 2024).

Chagué, Alix et Thiabult Clérice, « Données ouvertes, données propres, et autres vies : Testaments de Poilus et CREMMA », 2023, https://inria.hal.science/hal-04347066 (page accédée le 19 janvier 2024).

Chagué, Alix et Thibault Clérice, *HTR-United*, https://htr-united.github.io/ (page accédée le 18 octobre 2024).

UNESCO, « Recommandation de l’UNESCO sur une science ouverte », 2021, DOI : 10.54677/LTRF8541.