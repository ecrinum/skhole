---
title: "Le *single source publishing* - Antoine Fauchié"
numero: 2
date: 2023-11-28T02:01:58+05:30

# description brève (~150 caractères)
description: "Ou la publication à source unique"

# paragraphe de présentation (multiligne accepté)
description_longue: |
  L’édition numérique n’est pas que la pratique informatisée de l’édition papier. Les versions multiples et formats divers impliqués dans l’édition numérique peuvent rapidement embrouiller et faire dysfonctionner la chaîne éditoriale. Centrale à ce phénomène, la notion de single source publishing, ou de « publication à source unique », permet de décrire et de repenser les défis liés au versionnement, au partage des épreuves et à la pérennité.

# étiquettes
tags: [edition, formats, interoperabilite, fabrique]
# chemin vers le fichier mp3
mp3: "/medias/ep2_fauchie.mp3"
# taille en octets pour le fichier mp3 (facultatif)
length: "55557273"
# URL pour lecteur Spotify embarqué
spotify_embed_url: "https://podcasters.spotify.com/pod/show/ecrinum/embed/episodes/2--Publication--source-unique---Antoine-Fauchi-e299djl/a-aabot1f"
# URL complet pour Apple Podcasts
apple_url: "https://podcasts.apple.com/ca/podcast/2-publication-%C3%A0-source-unique-antoine-fauchi%C3%A9/id1689113816?i=1000636826330"
---

## Biographie

Antoine Fauchié est doctorant en littérature, option humanités numériques au Département de littératures et de langues du monde à l’Université de Montréal. Ses recherches portent sur la reconfiguration des processus de publication et l’évolution des pratiques d’écriture et d’édition dans le champ littéraire. Il a dirigé et participé à de nombreux projets d’édition numérique, tels que le [Novendécaméron](https://novendecameron.ramures.org) et le développement de l’éditeur de texte sémantique [Stylo](https://stylo.huma-num.fr). Ses recherches sont recensées dans son carnet : [www.quaternum.net](https://www.quaternum.net).

## Ressources et références

Fauchié, A. et Audin, Y. (2023). The Importance of Single Source Publishing in Scientific Publishing. _Digital Studies / Le Champ Numérique_. https://doi.org/10.16995/dscn.9655

Fauchié, A. (2021). Single source publishing avec Stylo (Rapport de recherche). Chaire de recherche du Canada sur les écritures numériques. http://revue20.org/les-publications/rapport-de-recherche-single-source-publishing-avec-stylo/


